package repositories.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.User;

public class UserRetriever implements IEntityRetriever<User> {

	public User build(ResultSet rs) throws SQLException {
		User result = new User();
		result.setId(rs.getInt("id"));
		result.setUsername(rs.getString("username"));
		result.setPassword(rs.getString("password"));
		return result;
	}

}