package repositories.impl;

import java.sql.Connection;

import repositories.*;
import domain.*;
import unitofwork.IUnitOfWork;


public abstract class RepositoryCatalog implements IRepository{

	private Connection connection;
	private IUnitOfWork uow;
	
	public RepositoryCatalog(Connection connection, IUnitOfWork uow) {
		super();
		this.connection = connection;
		this.uow = uow;
	}

	
	public IUserRepository getUsers() {
		return new UserRepository(connection, new UserRetriever(), uow);
	}


	
	public void commit() {
		uow.commit();
	}

	
	public IRepository<EnumerationValue> getEnumerationValue() {

		return new EnumerationValueRepository(connection, 
				new EnumerationValueRetriever(), uow);
	}

	
	
	public IRepository<User> getUser() {
		return new UserRepository(connection,
				new UserRetriever(), uow);
	}

	
	public IRepository<Person> getPerson() {
		// TODO Auto-generated method stub
		return null;
	}

}